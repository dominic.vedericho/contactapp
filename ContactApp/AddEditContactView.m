//
//  AddEditContactContactView.m
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "AddEditContactView.h"

@interface AddEditContactView ()

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) RNImageView *profileImageView;
@property (strong, nonatomic) UIView *pickImageSourceView;
@property (strong, nonatomic) UIImageView *pickImageIconImageView;

@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;

@end

@interface AddEditContactView ()

@end

@implementation AddEditContactView

#pragma mark - Lifecycle
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), 235.0f)];
        self.headerView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self addSubview:self.headerView];
        
        UIView *gradientView = [[UIView alloc] initWithFrame:self.headerView.frame];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        gradient.frame = gradientView.bounds;
        gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[Util getColor:@"E2F7F3"].CGColor, (id)[[Util getColor:@"D9f5EE"] colorWithAlphaComponent:0.5f].CGColor];
        
        [self.headerView.layer insertSublayer:gradient atIndex:0];

        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(6.0f, 40.0f, 65.0f, 30.0f)];
        [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"") forState:UIControlStateNormal];
        [self.cancelButton setTitleColor:[Util getColor:@"50E3C2"] forState:UIControlStateNormal];
        [self.headerView addSubview:self.cancelButton];
        
        _doneButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.headerView.frame) - 50.0f - 6.0f, 40.0f, 50.0f, 30.0f)];
        [self.doneButton setTitle:NSLocalizedString(@"Done", @"") forState:UIControlStateNormal];
        [self.doneButton setTitleColor:[Util getColor:@"50E3C2"] forState:UIControlStateNormal];
        [self.headerView addSubview:self.doneButton];
        
        _profileImageView = [[RNImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.headerView.frame) - 111.0f) / 2.0f, 84.0f, 111.0f, 111.0f)];
        self.profileImageView.layer.cornerRadius = CGRectGetHeight(self.profileImageView.frame) / 2.0f;
        self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.profileImageView.layer.borderWidth = 3.0f;
        self.profileImageView.clipsToBounds = YES;
        self.profileImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.profileImageView.image = [UIImage imageNamed:@"defaultPict"];
        [self.headerView addSubview:self.profileImageView];
        
        _pickImageSourceView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.profileImageView.frame) - 33.0f, CGRectGetMaxY(self.profileImageView.frame) - 33.0f, 33.0f, 33.0f)];
        self.pickImageSourceView.layer.cornerRadius = CGRectGetHeight(self.pickImageSourceView.frame) / 2.0f;
        self.pickImageSourceView.clipsToBounds = YES;
        self.pickImageSourceView.backgroundColor = [UIColor whiteColor];
        [self.headerView addSubview:self.pickImageSourceView];
        
        _pickImageIconImageView = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.pickImageSourceView.frame) - 20.0f) / 2.0f, (CGRectGetHeight(self.pickImageSourceView.frame) - 20.0f) / 2.0f, 20.0f, 20.0f)];
        self.pickImageIconImageView.image = [UIImage imageNamed:@"cameraGreen"];
        self.pickImageIconImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.pickImageSourceView addSubview:self.pickImageIconImageView];
        
        _pickImageButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.pickImageSourceView.frame) - 5.0f, CGRectGetMinY(self.pickImageSourceView.frame) - 5.0f, 43.0f, 43.0f)];
        self.pickImageButton.layer.cornerRadius = CGRectGetHeight(self.pickImageButton.frame) / 2.0f;
        [self.headerView addSubview:self.pickImageButton];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, CGRectGetMaxY(self.headerView.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) - CGRectGetHeight(self.headerView.frame)) style:UITableViewStyleGrouped];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self addSubview:self.tableView];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
        self.loadingView.backgroundColor = [Util getColor:@"F9F9F9"];
        self.loadingView.alpha = 0.0f;
        [self addSubview:self.loadingView];
        
        _indicatorView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.loadingView.frame) - 20.0f) / 2.0f, (CGRectGetHeight(self.loadingView.frame) - 20.0f) / 2.0f, 20.0f, 20.0f)];
        self.indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.indicatorView startAnimating];
        [self.loadingView addSubview:self.indicatorView];
    }
    
    return self;
}

#pragma mark - Custom Method
- (void)setProfilePictureWithImage:(UIImage *)image {
    if(image == nil) {
        self.profileImageView.image = [UIImage imageNamed:@"defaultPict"];
    }
    else {
        self.profileImageView.image = image;
    }
}

- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated {
    if (isAnimated) {
        if (isLoading) {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 1.0f;
            }];
        }
        else {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 0.0f;
            }];
        }
    }
    else {
        if (isLoading) {
            self.loadingView.alpha = 1.0f;
        }
        else {
            self.loadingView.alpha = 0.0f;
        }
    }
}

@end
