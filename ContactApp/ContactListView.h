//
//  ContactListView.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactListView : BaseView

@property (strong, nonatomic) UITableView *tableView;

- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated;

@end

NS_ASSUME_NONNULL_END
