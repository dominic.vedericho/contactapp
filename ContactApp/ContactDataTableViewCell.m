//
//  ContactDataTableViewCell.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactDataTableViewCell.h"

@interface ContactDataTableViewCell ()

@property (strong, nonatomic) UILabel *placeholderLabel;
@property (strong, nonatomic) UILabel *dataLabel;
@property (strong, nonatomic) UIView *separatorView;

@end

@implementation ContactDataTableViewCell

#pragma mark - Lifecycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        self.contentView.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(24.0f, 0.0f, 50.0f, 55.0f)];
        self.placeholderLabel.font = [UIFont systemFontOfSize:16.0f];
        self.placeholderLabel.textColor = [[Util getColor:@"4A4A4A"] colorWithAlphaComponent:0.5f];
        [self.contentView addSubview:self.placeholderLabel];
        
        CGFloat dataLabelWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - CGRectGetMaxX(self.placeholderLabel.frame) - 32.0f - 24.0f;
        _dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.placeholderLabel.frame) + 32.0f, 0.0f, dataLabelWidth, 55.0f)];
        self.dataLabel.font = [UIFont systemFontOfSize:16.0f];
        self.dataLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.contentView addSubview:self.dataLabel];
        
        //Screen width - left gap
        CGFloat separatorWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - 8.0f;
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(8.0f, 56.0f - 1.0f, separatorWidth , 1.0f)];
        self.separatorView.backgroundColor = [Util getColor:@"F0F0F0"];
        [self.contentView addSubview:self.separatorView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.placeholderLabel.text = @"";
    self.dataLabel.text = @"";
}

#pragma mark - Custom Method
- (void)setContactCellWithData:(ContactModel *)contact type:(ContactDataTableViewCellType)type {
    if (type == ContactDataTableViewCellTypePhone) {
        self.placeholderLabel.text = NSLocalizedString(@"Mobile", @"");
        
        NSString *phoneNumber = contact.phoneNumber;
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        if ([phoneNumber isEqualToString:@""]) {
            self.dataLabel.text = @"-";
        }
        else {
            self.dataLabel.text = phoneNumber;
        }
    }
    else if (type == ContactDataTableViewCellTypeEmail) {
        self.placeholderLabel.text = NSLocalizedString(@"Email", @"");
        
        NSString *email = contact.email;
        email = [Util nullToEmptyString:email];
        
        if ([email isEqualToString:@""]) {
            self.dataLabel.text = @"-";
        }
        else {
            self.dataLabel.text = email;
        }
    }
}

@end
