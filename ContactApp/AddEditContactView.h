//
//  AddEditContactView.h
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddEditContactView : BaseView

@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UIButton *pickImageButton;

@property (strong, nonatomic) UITableView *tableView;

- (void)setProfilePictureWithImage:(UIImage *)image;
- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated;

@end

NS_ASSUME_NONNULL_END
