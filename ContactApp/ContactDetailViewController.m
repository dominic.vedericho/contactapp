//
//  ContactDetailViewController.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "ContactDetailView.h"
#import "ContactDataTableViewCell.h"
#import "AddEditContactViewController.h"

#import <MessageUI/MessageUI.h>

@interface ContactDetailViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, AddEditContactViewControllerDelegate>

@property (strong, nonatomic) ContactDetailView *contactDetailView;
@property (strong, nonatomic) ContactModel *contactData;

- (void)backButtonDidTapped;
- (void)editButtonDidTapped;
- (void)messageButtonDidTapped;
- (void)phoneButtonDidTapped;
- (void)emailButtonDidTapped;
- (void)favoriteButtonDidTapped;
- (void)fetchDetailDataFromAPIWithNeedLoading:(BOOL)isNeedLoading;

@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _contactDetailView = [[ContactDetailView alloc] initWithFrame:[BaseView frameWithoutNavigationBar]];
    [self.view addSubview:self.contactDetailView];
    
    self.contactDetailView.contactDataTableView.delegate = self;
    self.contactDetailView.contactDataTableView.dataSource = self;
    
    [self.contactDetailView.editButton addTarget:self action:@selector(editButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contactDetailView.backButton addTarget:self action:@selector(backButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contactDetailView.messageButton addTarget:self action:@selector(messageButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contactDetailView.phoneButton addTarget:self action:@selector(phoneButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contactDetailView.emailButton addTarget:self action:@selector(emailButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contactDetailView.favoriteButton addTarget:self action:@selector(favoriteButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self fetchDetailDataFromAPIWithNeedLoading:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Data Source
#pragma mark TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"ContactDataTableViewCell";
    ContactDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[ContactDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if(self.contactData != nil) {
        if (indexPath.row == 0) {
            // Phone
            [cell setContactCellWithData:self.contactData type:ContactDataTableViewCellTypePhone];
        }
        else if (indexPath.row == 1) {
            // Email
            [cell setContactCellWithData:self.contactData type:ContactDataTableViewCellTypeEmail];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] init];
    return footerView;
}

#pragma mark - Delegate
#pragma mark UITableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark MFMailComposeViewController
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            //Email sent
            break;
        case MFMailComposeResultSaved:
            //Email saved
            break;
        case MFMailComposeResultCancelled:
            //Handle cancelling of the email
            break;
        case MFMailComposeResultFailed:
            //Handle failure to send.
            break;
        default:
            //A failure occurred while completing the email
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark MFMessageComposeViewController
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark AddEditContactViewController
- (void)doneAddOrEditContact {
    
    //Refresh detail view
    [self fetchDetailDataFromAPIWithNeedLoading:YES];
    
    if ([self.delegate respondsToSelector:@selector(doneAddOrEditDataFromContactDetail)]) {
        [self.delegate doneAddOrEditDataFromContactDetail];
    }
}

#pragma mark - Custom Method
- (void)backButtonDidTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)editButtonDidTapped {
    AddEditContactViewController *addEditContactViewController = [[AddEditContactViewController alloc] init];
    addEditContactViewController.delegate = self;
    addEditContactViewController.contactID = self.contactID;
    [addEditContactViewController setAddEditContactViewControllerType:AddEditContactViewControllerTypeEdit];
    [self presentViewController:addEditContactViewController animated:YES completion:nil];
}

- (void)messageButtonDidTapped {
    NSString *phoneNumber = self.contactData.phoneNumber;
    
    if ([MFMessageComposeViewController canSendText] && ![phoneNumber isEqualToString:@""]) {
    NSArray *recipientArray = @[phoneNumber];
    NSString *message = @"Lorem Ipsum";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipientArray];
    [messageController setBody:message];
    
    [self presentViewController:messageController animated:YES completion:nil];
    }
    else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Phone number not found or can not send SMS" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)phoneButtonDidTapped {
    NSString *phoneNumber = self.contactData.phoneNumber;
    NSString *urlString = [NSString stringWithFormat:@"tel:%@",phoneNumber];
    
    if([phoneNumber isEqualToString:@""]) {
        //Not contain phone number
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Phone number not found" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
    
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        if(IS_IOS_10_OR_ABOVE) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString] options:[NSDictionary dictionary] completionHandler:nil];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
    }
}

- (void)emailButtonDidTapped {
    
    NSString *email = self.contactData.email;
    
    if ([MFMailComposeViewController canSendMail] && ![email isEqualToString:@""]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        NSArray *recipientArray = @[email];
        [mailViewController setToRecipients:recipientArray];
        [mailViewController setSubject:@"Lorem Ipsum"];
        mailViewController.mailComposeDelegate = self;
        [self presentViewController:mailViewController animated:YES completion:nil];
    } else {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Email not found or Email has not configured" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)favoriteButtonDidTapped {
    
    [self.contactDetailView setAsLoading:YES animated:YES];
    
    BOOL isFavorite = self.contactData.isFavorite;
    
    //Call reverse of is favorite data
    [DataManager callAPIMarkAsFavourite:!isFavorite contactID:self.contactID success:^(ContactModel *contactData) {
        
        //Save to database contact
        [DataManager updateOrInsertContactToDatabaseWithData:@[contactData] tableName:@"ContactRealmModel" success:^{
            //Refresh detail view
            [self fetchDetailDataFromAPIWithNeedLoading:NO];
            
            if ([self.delegate respondsToSelector:@selector(doneAddOrEditDataFromContactDetail)]) {
                [self.delegate doneAddOrEditDataFromContactDetail];
            }
        } failure:^(NSError *error) {
            
        }];
        
    } failure:^(NSError *error) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        [self.contactDetailView setAsLoading:NO animated:YES];
    }];
}

- (void)fetchDetailDataFromAPIWithNeedLoading:(BOOL)isNeedLoading {
    
    if (isNeedLoading) {
        [self.contactDetailView setAsLoading:YES animated:NO];
    }
    
    [DataManager callAPIGetContactDetailWithContactID:self.contactID success:^(ContactModel *contactData) {
        self.contactData = contactData;
        [self.contactDetailView setContactDetailViewWithData:self.contactData];
        [self.contactDetailView.contactDataTableView reloadData];
        
        if (self.contactDetailView.contactDataTableView.contentSize.height < self.contactDetailView.contactDataTableView.frame.size.height) {
            self.contactDetailView.contactDataTableView.scrollEnabled = NO;
        }
        else {
            self.contactDetailView.contactDataTableView.scrollEnabled = YES;
        }
        
        [self.contactDetailView setAsLoading:NO animated:YES];
    } failure:^(NSError *error) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        [self.contactDetailView setAsLoading:NO animated:YES];
    }];
}

@end
