//
//  DataManager.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "DataManager.h"
#import "APIManager.h"
#import "NetworkManager.h"
#import "DatabaseManager.h"
#import "AppDelegate.h"

#define kDatabaseTableContact @"ContactRealmModel"

@interface DataManager ()

+ (void)logErrorStringFromError:(NSError *)error;
+ (ContactModel *)contactModelFromDictionary:(NSDictionary *)dictionary;
+ (NSDictionary *)dictionaryFromContactModel:(ContactModel *)contact;

@end

@implementation DataManager

#pragma mark - Lifecycle


#pragma mark - Custom Method
+ (void)logErrorStringFromError:(NSError *)error {
    NSString *dataString = [[NSString alloc] initWithData:[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
#if DEBUG
    NSLog(@"Error Response: %@", dataString);
#endif
}

+ (ContactModel *)contactModelFromDictionary:(NSDictionary *)dictionary {
    dictionary = [Util nullToEmptyDictionary:dictionary];
    ContactModel *contact = [ContactModel new];
    
    NSString *contactID = [dictionary objectForKey:@"contactID"];
    contactID = [Util nullToEmptyString:contactID];
    contact.contactID = contactID;
    
    NSString *firstName = [dictionary objectForKey:@"firstName"];
    firstName = [Util nullToEmptyString:firstName];
    contact.firstName = firstName;
    
    NSString *lastName = [dictionary objectForKey:@"lastName"];
    lastName = [Util nullToEmptyString:lastName];
    contact.lastName = lastName;
    
    NSString *phoneNumber = [dictionary objectForKey:@"phoneNumber"];
    phoneNumber = [Util nullToEmptyString:phoneNumber];
    contact.phoneNumber = phoneNumber;
    
    NSString *email = [dictionary objectForKey:@"email"];
    email = [Util nullToEmptyString:email];
    contact.email = email;
    
    NSString *profilePicture = [dictionary objectForKey:@"profilePicture"];
    profilePicture = [Util nullToEmptyString:profilePicture];
    contact.profilePicture = profilePicture;
    
    NSNumber *isFavorite = [dictionary objectForKey:@"isFavorite"];
    BOOL isFav = [isFavorite boolValue];
    contact.isFavorite = isFav;
    
    NSString *urlString = [dictionary objectForKey:@"urlString"];
    urlString = [Util nullToEmptyString:urlString];
    contact.urlString = urlString;
    
    return contact;
}

+ (NSDictionary *)dictionaryFromContactModel:(ContactModel *)contact {
    NSString *contactID = contact.contactID;
    contactID = [Util nullToEmptyString:contactID];
    
    NSString *firstName = contact.firstName;
    firstName = [Util nullToEmptyString:firstName];
    
    NSString *lastName = contact.lastName;
    lastName = [Util nullToEmptyString:lastName];
    
    NSString *email = contact.email;
    email = [Util nullToEmptyString:email];
    
    NSString *phoneNumber = contact.phoneNumber;
    phoneNumber = [Util nullToEmptyString:phoneNumber];
    
    NSString *profilePicture = contact.profilePicture;
    profilePicture = [Util nullToEmptyString:profilePicture];
    
    BOOL isFavourite = contact.isFavorite;
    
    NSString *urlString = contact.urlString;
    urlString = [Util nullToEmptyString:urlString];
    
    NSMutableDictionary *contactDictionary = [[NSMutableDictionary alloc] init];

    [contactDictionary setValue:contactID forKey:@"contactID"];
    [contactDictionary setValue:firstName forKey:@"firstName"];
    [contactDictionary setValue:lastName forKey:@"lastName"];
    [contactDictionary setValue:email forKey:@"email"];
    [contactDictionary setValue:phoneNumber forKey:@"phoneNumber"];
    [contactDictionary setValue:profilePicture forKey:@"profilePicture"];
    [contactDictionary setValue:[NSNumber numberWithBool:isFavourite] forKey:@"isFavorite"];
    [contactDictionary setValue:urlString forKey:@"urlString"];
    
    return contactDictionary;
}

#pragma mark API Call
+ (void)callAPIGetContactListSuccess:(void (^)(NSArray<ContactModel *> *contactListArray))success
                             failure:(void (^)(NSError *error))failure {
    
    NSString *baseRequestURL = [APIManager urlForType:APIManagerTypeContact];
    NSString *requestURL = [NSString stringWithFormat:@"%@.json", baseRequestURL];
    
    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionary];
    
    [[NetworkManager sharedManager] get:requestURL parameters:parameterDictionary progress:^(NSProgress *uploadProgress) {
        
    } success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        
        NSMutableArray *contactListArray = [NSMutableArray array];
        for (NSDictionary *contactDictionary in responseObject) {
            NSString *contactIDRaw = [contactDictionary objectForKey:@"id"];
            contactIDRaw = [Util nullToEmptyString:contactIDRaw];
            NSString *contactID = [NSString stringWithFormat:@"%ld", (long)[contactIDRaw longLongValue]];
            
            NSString *firstName = [contactDictionary objectForKey:@"first_name"];
            firstName = [Util nullToEmptyString:firstName];
            
            NSString *lastName = [contactDictionary objectForKey:@"last_name"];
            lastName = [Util nullToEmptyString:lastName];
            
            NSString *profilePicture = [contactDictionary objectForKey:@"profile_pic"];
            profilePicture = [Util nullToEmptyString:profilePicture];
            
            NSString *email = [contactDictionary objectForKey:@"email"];
            email = [Util nullToEmptyString:email];
            
            NSString *phoneNumber = [contactDictionary objectForKey:@"phone_number"];
            phoneNumber = [Util nullToEmptyString:phoneNumber];
            
            NSNumber *favorite = [contactDictionary objectForKey:@"favorite"];
            BOOL isFavorite = [favorite boolValue];
            
            NSString *urlString = [contactDictionary objectForKey:@"url"];
            urlString = [Util nullToEmptyString:urlString];
            
            ContactModel *contact = [ContactModel new];
            contact.contactID = contactID;
            contact.firstName = firstName;
            contact.lastName = lastName;
            contact.profilePicture = profilePicture;
            contact.isFavorite = isFavorite;
            contact.urlString = urlString;
            contact.phoneNumber = phoneNumber;
            contact.email = email;

            
            [contactListArray addObject:contact];
        }
        
        success(contactListArray);
    
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        [DataManager logErrorStringFromError:error];

        NSError *localizedError = [NSError errorWithDomain:NSLocalizedString(@"We are experiencing problem to connect to our server, please try again later...", @"") code:999 userInfo:@{@"message": NSLocalizedString(@"Failed to connect to our server, please try again later...", @"")}];
        
        failure(localizedError);
    }];
}

+ (void)callAPIGetContactDetailWithContactID:(NSString *)contactID
                                     success:(void (^)(ContactModel *contactData))success
                                     failure:(void (^)(NSError *error))failure {
    
    NSString *baseRequestURL = [APIManager urlForType:APIManagerTypeContact];
    long contactIDValue = [contactID longLongValue];
    NSString *requestURL = [NSString stringWithFormat:@"%@/%@.json", baseRequestURL, [NSNumber numberWithLong:contactIDValue]];
    
    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionary];
    
    [[NetworkManager sharedManager] get:requestURL parameters:parameterDictionary progress:^(NSProgress *uploadProgress) {
        
    } success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        
        NSString *contactIDRaw = [responseObject objectForKey:@"id"];
        contactIDRaw = [Util nullToEmptyString:contactIDRaw];
        NSString *contactID = [NSString stringWithFormat:@"%ld", (long)[contactIDRaw longLongValue]];
        
        NSString *firstName = [responseObject objectForKey:@"first_name"];
        firstName = [Util nullToEmptyString:firstName];
        
        NSString *lastName = [responseObject objectForKey:@"last_name"];
        lastName = [Util nullToEmptyString:lastName];
        
        NSString *profilePicture = [responseObject objectForKey:@"profile_pic"];
        profilePicture = [Util nullToEmptyString:profilePicture];
        
        NSString *email = [responseObject objectForKey:@"email"];
        email = [Util nullToEmptyString:email];
        
        NSString *phoneNumber = [responseObject objectForKey:@"phone_number"];
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        NSNumber *favorite = [responseObject objectForKey:@"favorite"];
        BOOL isFavorite = [favorite boolValue];
        
        NSString *urlString = [responseObject objectForKey:@"url"];
        urlString = [Util nullToEmptyString:urlString];
        
        ContactModel *contact = [ContactModel new];
        contact.contactID = contactID;
        contact.firstName = firstName;
        contact.lastName = lastName;
        contact.profilePicture = profilePicture;
        contact.isFavorite = isFavorite;
        contact.urlString = urlString;
        contact.phoneNumber = phoneNumber;
        contact.email = email;
        
        success(contact);
        
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        [DataManager logErrorStringFromError:error];
        
        NSError *localizedError = [NSError errorWithDomain:NSLocalizedString(@"We are experiencing problem to connect to our server, please try again later...", @"") code:999 userInfo:@{@"message": NSLocalizedString(@"Failed to connect to our server, please try again later...", @"")}];
        
        failure(localizedError);
    }];
}

+ (void)callAPIAddNewContactWithData:(ContactModel *)contact
                             success:(void (^)(ContactModel *contactData))success
                             failure:(void (^)(NSError *error))failure {
    
    NSString *baseRequestURL = [APIManager urlForType:APIManagerTypeContact];
    NSString *requestURL = [NSString stringWithFormat:@"%@.json", baseRequestURL];
    
    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionary];
    [parameterDictionary setObject:contact.firstName forKey:@"first_name"];
    [parameterDictionary setObject:contact.lastName forKey:@"last_name"];
    [parameterDictionary setObject:contact.email forKey:@"email"];
    [parameterDictionary setObject:contact.phoneNumber forKey:@"phone_number"];
    [parameterDictionary setObject:[NSNumber numberWithBool:contact.isFavorite] forKey:@"favorite"];
    
    [[NetworkManager sharedManager] post:requestURL parameters:parameterDictionary progress:^(NSProgress *uploadProgress) {
        
    } success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        
        NSString *contactIDRaw = [responseObject objectForKey:@"id"];
        contactIDRaw = [Util nullToEmptyString:contactIDRaw];
        NSString *contactID = [NSString stringWithFormat:@"%ld", (long)[contactIDRaw longLongValue]];
        
        NSString *firstName = [responseObject objectForKey:@"first_name"];
        firstName = [Util nullToEmptyString:firstName];
        
        NSString *lastName = [responseObject objectForKey:@"last_name"];
        lastName = [Util nullToEmptyString:lastName];
        
        NSString *profilePicture = [responseObject objectForKey:@"profile_pic"];
        profilePicture = [Util nullToEmptyString:profilePicture];
        
        NSString *email = [responseObject objectForKey:@"email"];
        email = [Util nullToEmptyString:email];
        
        NSString *phoneNumber = [responseObject objectForKey:@"phone_number"];
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        NSNumber *favorite = [responseObject objectForKey:@"favorite"];
        BOOL isFavorite = [favorite boolValue];
        
        NSString *urlString = [responseObject objectForKey:@"url"];
        urlString = [Util nullToEmptyString:urlString];
        
        ContactModel *contact = [ContactModel new];
        contact.contactID = contactID;
        contact.firstName = firstName;
        contact.lastName = lastName;
        contact.profilePicture = profilePicture;
        contact.isFavorite = isFavorite;
        contact.urlString = urlString;
        contact.phoneNumber = phoneNumber;
        contact.email = email;
        
        success(contact);
        
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        [DataManager logErrorStringFromError:error];
        
        NSError *localizedError = [NSError errorWithDomain:NSLocalizedString(@"We are experiencing problem to connect to our server, please try again later...", @"") code:999 userInfo:@{@"message": NSLocalizedString(@"Failed to connect to our server, please try again later...", @"")}];
        
        failure(localizedError);
    }];
}

+ (void)callAPIUpdateContactWithContactID:(NSString *)contactID
                              contactData:(ContactModel *)contact
                                  success:(void (^)(ContactModel *contactData))success
                                  failure:(void (^)(NSError *error))failure {
    
    NSString *baseRequestURL = [APIManager urlForType:APIManagerTypeContact];
    long contactIDValue = [contactID longLongValue];
    NSString *requestURL = [NSString stringWithFormat:@"%@/%@.json", baseRequestURL, [NSNumber numberWithLong:contactIDValue]];
    
    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionary];
    [parameterDictionary setObject:contact.firstName forKey:@"first_name"];
    [parameterDictionary setObject:contact.lastName forKey:@"last_name"];
    [parameterDictionary setObject:contact.email forKey:@"email"];
    [parameterDictionary setObject:contact.phoneNumber forKey:@"phone_number"];
    [parameterDictionary setObject:[NSNumber numberWithBool:contact.isFavorite] forKey:@"favorite"];
    
    [[NetworkManager sharedManager] put:requestURL parameters:parameterDictionary success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        
        NSString *contactIDRaw = [responseObject objectForKey:@"id"];
        contactIDRaw = [Util nullToEmptyString:contactIDRaw];
        NSString *contactID = [NSString stringWithFormat:@"%ld", (long)[contactIDRaw longLongValue]];
        
        NSString *firstName = [responseObject objectForKey:@"first_name"];
        firstName = [Util nullToEmptyString:firstName];
        
        NSString *lastName = [responseObject objectForKey:@"last_name"];
        lastName = [Util nullToEmptyString:lastName];
        
        NSString *profilePicture = [responseObject objectForKey:@"profile_pic"];
        profilePicture = [Util nullToEmptyString:profilePicture];
        
        NSString *email = [responseObject objectForKey:@"email"];
        email = [Util nullToEmptyString:email];
        
        NSString *phoneNumber = [responseObject objectForKey:@"phone_number"];
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        NSNumber *favorite = [responseObject objectForKey:@"favorite"];
        BOOL isFavorite = [favorite boolValue];
        
        NSString *urlString = [responseObject objectForKey:@"url"];
        urlString = [Util nullToEmptyString:urlString];
        
        ContactModel *contact = [ContactModel new];
        contact.contactID = contactID;
        contact.firstName = firstName;
        contact.lastName = lastName;
        contact.profilePicture = profilePicture;
        contact.isFavorite = isFavorite;
        contact.urlString = urlString;
        contact.phoneNumber = phoneNumber;
        contact.email = email;
        
        success(contact);
        
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        [DataManager logErrorStringFromError:error];
        
        NSError *localizedError = [NSError errorWithDomain:NSLocalizedString(@"We are experiencing problem to connect to our server, please try again later...", @"") code:999 userInfo:@{@"message": NSLocalizedString(@"Failed to connect to our server, please try again later...", @"")}];
        
        failure(localizedError);
    }];
}

+ (void)callAPIMarkAsFavourite:(BOOL)setAsFavourite
                     contactID:(NSString *)contactID
                       success:(void (^)(ContactModel *contactData))success
                       failure:(void (^)(NSError *error))failure {
    
    NSString *baseRequestURL = [APIManager urlForType:APIManagerTypeContact];
    long contactIDValue = [contactID longLongValue];
    NSString *requestURL = [NSString stringWithFormat:@"%@/%@.json", baseRequestURL, [NSNumber numberWithLong:contactIDValue]];
    
    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionary];
    [parameterDictionary setObject:[NSNumber numberWithBool:setAsFavourite] forKey:@"favorite"];
    
    [[NetworkManager sharedManager] put:requestURL parameters:parameterDictionary success:^(NSURLSessionDataTask *dataTask, NSDictionary *responseObject) {
        
        NSString *contactIDRaw = [responseObject objectForKey:@"id"];
        contactIDRaw = [Util nullToEmptyString:contactIDRaw];
        NSString *contactID = [NSString stringWithFormat:@"%ld", (long)[contactIDRaw longLongValue]];
        
        NSString *firstName = [responseObject objectForKey:@"first_name"];
        firstName = [Util nullToEmptyString:firstName];
        
        NSString *lastName = [responseObject objectForKey:@"last_name"];
        lastName = [Util nullToEmptyString:lastName];
        
        NSString *profilePicture = [responseObject objectForKey:@"profile_pic"];
        profilePicture = [Util nullToEmptyString:profilePicture];
        
        NSString *email = [responseObject objectForKey:@"email"];
        email = [Util nullToEmptyString:email];
        
        NSString *phoneNumber = [responseObject objectForKey:@"phone_number"];
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        NSNumber *favorite = [responseObject objectForKey:@"favorite"];
        BOOL isFavorite = [favorite boolValue];
        
        NSString *urlString = [responseObject objectForKey:@"url"];
        urlString = [Util nullToEmptyString:urlString];
        
        ContactModel *contact = [ContactModel new];
        contact.contactID = contactID;
        contact.firstName = firstName;
        contact.lastName = lastName;
        contact.profilePicture = profilePicture;
        contact.isFavorite = isFavorite;
        contact.urlString = urlString;
        contact.phoneNumber = phoneNumber;
        contact.email = email;
        
        success(contact);
        
    } failure:^(NSURLSessionDataTask *dataTask, NSError *error) {
        [DataManager logErrorStringFromError:error];
        
        NSError *localizedError = [NSError errorWithDomain:NSLocalizedString(@"We are experiencing problem to connect to our server, please try again later...", @"") code:999 userInfo:@{@"message": NSLocalizedString(@"Failed to connect to our server, please try again later...", @"")}];
        
        failure(error);
    }];
}

#pragma mark Database Call
+ (void)getDatabaseContactListWithSuccess:(void (^)(NSArray *contactListDatabaseArray))success
                                  failure:(void (^)(NSError *error))failure {
    [DatabaseManager loadAllDataFromDatabaseWithQuery:@"" tableName:kDatabaseTableContact sortByKey:@"firstName" ascending:YES success:^(NSArray *resultArray) {
        
        resultArray = [Util nullToEmptyArray:resultArray];
        
        NSMutableArray *obtainedArray = [NSMutableArray array];
        for (NSDictionary *databaseDictionary in resultArray) {
            ContactModel *contact = [ContactModel new];
            contact = [DataManager contactModelFromDictionary:databaseDictionary];
            [obtainedArray addObject:contact];
        }
        
        success(obtainedArray);
        
    } failure:^(NSError *error) {
        failure(error);
    }];
}

+ (void)updateOrInsertContactToDatabaseWithData:(NSArray *)dataArray
                                      tableName:(NSString *)tableName
                                      success:(void (^)(void))success
                                      failure:(void (^)(NSError *error))failure {
    if ([dataArray count] <= 0) {
        success();
    }
    
    NSMutableArray *contactDictionaryArray = [NSMutableArray array];
    for (ContactModel *contact in dataArray) {
        NSDictionary *contactDictionary = [DataManager dictionaryFromContactModel:contact];
        contactDictionary = [Util nullToEmptyDictionary:contactDictionary];
        
        [contactDictionaryArray addObject:contactDictionary];
    }
    
    [DatabaseManager updateOrInsertDataToDatabaseWithData:contactDictionaryArray tableName:tableName success:^{
        success();
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
