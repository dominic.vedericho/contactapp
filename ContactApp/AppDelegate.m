//
//  AppDelegate.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self firstRunSetupWithApplication:application launchOptions:launchOptions];
    
    _window = [[UIWindow alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
    
    //Initialize view controller
    _contactListViewController = [[ContactListViewController alloc] init];
    UINavigationController *contactListNavigationController = [[UINavigationController alloc] initWithRootViewController:self.contactListViewController];
    
    self.window.rootViewController = contactListNavigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - First Run Setup
- (void)firstRunSetupWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions {
    
    id versionObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"Prefs.appVersion"];
    
    NSString *versionFromPrefs = @"";
    
    if([versionObject isKindOfClass:[NSNumber class]]) {
        versionFromPrefs = [versionObject stringValue];
    }
    else if([versionObject isKindOfClass:[NSString class]]) {
        versionFromPrefs = versionObject;
    }
    
//    if(versionFromPrefs == nil || [versionFromPrefs isEqualToString:@""]) {
//        [self resetAllPersistentDatasAndRecreateDatabase:YES];
//    }
//    else {
//        [self initializeDatabase];
//    }
    
    //Other initialization
//    [[NSUserDefaults standardUserDefaults] setSecret:SECURE_KEY_NSUSERDEFAULTS]; //Set secret for NSSecureUserDefaults
}

//- (void)resetAllPersistentDatasAndRecreateDatabase:(BOOL)resetAllPersistent {
//    //Create New Database
//    if(resetAllPersistent){
//        [DatabaseManager createEditableCopyOfDatabaseIfNeeded];
//        [self resetPersistent];
//    }
//    else{
//        [DatabaseManager createEditableCopyOfDatabaseIfNeeded];
//    }
//
//    [self initializeDatabase];
//
//    //Assign APP_VERSION to Preferences
//    [[NSUserDefaults standardUserDefaults] setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"Prefs.appVersion"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}

//- (void)resetPersistent {
//    //Set initial refresh rate
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}

//#pragma mark Database
//- (void)initializeDatabase {
//    [DatabaseManager sharedManager]; //Trigger open database
//}


@end
