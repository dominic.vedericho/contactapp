//
//  APIManager.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"

typedef NS_ENUM(NSInteger, APIManagerType) {
    APIManagerTypeContact
};

@interface APIManager : BaseManager

+ (APIManager *)sharedManager;
+ (NSString *)urlForType:(APIManagerType)type;

@end
