//
//  APIManager.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "APIManager.h"

static NSString * const kAPIBaseURL = @"http://gojek-contacts-app.herokuapp.com";

@implementation APIManager

#pragma mark - Lifecycle
+ (APIManager *)sharedManager {
    static APIManager *sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[APIManager alloc] init];
    });
    return sharedManager;
}

#pragma mark - Custom Method
+ (NSString *)urlForType:(APIManagerType)type {
    if(type == APIManagerTypeContact) {
        NSString *apiPath = @"contacts";
        return [NSString stringWithFormat:@"%@/%@", kAPIBaseURL, apiPath];
    }
    
    return [NSString stringWithFormat:@"%@", kAPIBaseURL];
}

@end
