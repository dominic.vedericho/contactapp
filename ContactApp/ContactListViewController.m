//
//  ContactListViewController.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactListViewController.h"
#import "ContactListView.h"
#import "ContactListTableViewCell.h"

#import "ContactDetailViewController.h"
#import "AddEditContactViewController.h"

@interface ContactListViewController () <UITableViewDelegate, UITableViewDataSource, AddEditContactViewControllerDelegate, ContactDetailViewControllerDelegate>

@property (strong, nonatomic) ContactListView *contactListView;

@property (strong, nonatomic) NSArray *alphabetSectionTitles;
@property (strong, nonatomic) NSArray *contactListArray;
@property (strong, nonatomic) NSMutableDictionary *indexSectionDictionary;

@property (nonatomic) BOOL isLoading;

- (void)addButtonDidTapped;
- (void)fetchContactListDataFromDatabase;

@end

@implementation ContactListViewController

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [Util getColor:@"F9F9F9"];
    
    _contactListView = [[ContactListView alloc] initWithFrame:[BaseView frameWithNavigationBar]];
    self.contactListView.tableView.delegate = self;
    self.contactListView.tableView.dataSource = self;
    [self.view addSubview:self.contactListView];
    
    self.title = NSLocalizedString(@"Contacts", @"");
    
    UIImage *buttonImage = [UIImage imageNamed:@"addBlack"];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setRightBarButtonItem:barButtonItem];
    
    _alphabetSectionTitles = [NSArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];

    _contactListArray = [NSMutableArray array];
    _indexSectionDictionary = [NSMutableDictionary dictionary];
    _isLoading = NO;
    
    [self.contactListView setAsLoading:YES animated:NO];
    _isLoading = YES;
    //Get database contact
    [DataManager getDatabaseContactListWithSuccess:^(NSArray *contactListDatabaseArray) {
        
        NSArray *sortedDatabaseArray = [contactListDatabaseArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            ContactModel *contact1 = (ContactModel *)obj1;
            ContactModel *contact2 = (ContactModel *)obj2;
            
            NSString *firstName1 = contact1.firstName;
            firstName1 = [Util nullToEmptyString:firstName1];
            NSString *lastName1 = contact1.lastName;
            lastName1 = [Util nullToEmptyString:lastName1];
            NSString *fullName1 = [NSString stringWithFormat:@"%@ %@", firstName1, lastName1];
            
            NSString *firstName2 = contact2.firstName;
            firstName2 = [Util nullToEmptyString:firstName2];
            NSString *lastName2 = contact2.lastName;
            lastName2 = [Util nullToEmptyString:lastName2];
            NSString *fullName2 = [NSString stringWithFormat:@"%@ %@", firstName2, lastName2];
            
            return [fullName1 caseInsensitiveCompare:fullName2];
        }];
        
        if(sortedDatabaseArray == nil || [sortedDatabaseArray count] == 0) {
            //Database empty, call from API
            [DataManager callAPIGetContactListSuccess:^(NSArray<ContactModel *> *contactListArray) {
                
                NSArray *sortedArray = [contactListArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                
                    ContactModel *contact1 = (ContactModel *)obj1;
                    ContactModel *contact2 = (ContactModel *)obj2;
                    
                    NSString *firstName1 = contact1.firstName;
                    firstName1 = [Util nullToEmptyString:firstName1];
                    NSString *lastName1 = contact1.lastName;
                    lastName1 = [Util nullToEmptyString:lastName1];
                    NSString *fullName1 = [NSString stringWithFormat:@"%@ %@", firstName1, lastName1];
                    
                    NSString *firstName2 = contact2.firstName;
                    firstName2 = [Util nullToEmptyString:firstName2];
                    NSString *lastName2 = contact2.lastName;
                    lastName2 = [Util nullToEmptyString:lastName2];
                    NSString *fullName2 = [NSString stringWithFormat:@"%@ %@", firstName2, lastName2];
                    
                    return [fullName1 caseInsensitiveCompare:fullName2];
                }];
                
                self.contactListArray = sortedArray;
                
                //Save to database contact
                [DataManager updateOrInsertContactToDatabaseWithData:contactListArray tableName:@"ContactRealmModel" success:^{
                    
                    for (ContactModel *contact in self.contactListArray) {
                        
                        NSString *nameString = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
                        NSString *firstAlphabet = [[nameString substringWithRange:NSMakeRange(0, 1)] uppercaseString];
                        if ([self.alphabetSectionTitles containsObject:firstAlphabet]) {
                            if ([self.indexSectionDictionary objectForKey:firstAlphabet] == nil) {
                                //No alphabet found
                                [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                            }
                            else {
                                //Alphabet found
                                NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:firstAlphabet] mutableCopy];
                                [contactArray addObject:contact];
                                [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                            }
                        }
                        else {
                            if ([self.indexSectionDictionary objectForKey:@"#"] == nil) {
                                //No alphabet found
                                [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                            }
                            else {
                                //Alphabet found
                                NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:@"#"] mutableCopy];
                                [contactArray addObject:contact];
                                [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                            }
                        }
                    }
                    
                    [self.contactListView.tableView reloadData];
                    [self.contactListView setAsLoading:NO animated:YES];
                    self.isLoading = NO;
                } failure:^(NSError *error) {
                    [self.contactListView.tableView reloadData];
                    [self.contactListView setAsLoading:NO animated:YES];
                    self.isLoading = NO;
                }];
                
            } failure:^(NSError *error) {
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:okAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                [self.contactListView setAsLoading:NO animated:YES];
                self.isLoading = NO;
            }];
        }
        else {
            //Contain data from database
            self.contactListArray = sortedDatabaseArray;
            for (ContactModel *contact in self.contactListArray) {
                
                NSString *nameString = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
                NSString *firstAlphabet = [[nameString substringWithRange:NSMakeRange(0, 1)] uppercaseString];
                if ([self.alphabetSectionTitles containsObject:firstAlphabet]) {
                    if ([self.indexSectionDictionary objectForKey:firstAlphabet] == nil) {
                        //No alphabet found
                        [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                    }
                    else {
                        //Alphabet found
                        NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:firstAlphabet] mutableCopy];
                        [contactArray addObject:contact];
                        [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                    }
                }
                else {
                    if ([self.indexSectionDictionary objectForKey:@"#"] == nil) {
                        //No alphabet found
                        [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                    }
                    else {
                        //Alphabet found
                        NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:@"#"] mutableCopy];
                        [contactArray addObject:contact];
                        [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                    }
                }
            }
            
            [self.contactListView.tableView reloadData];
            [self.contactListView setAsLoading:NO animated:YES];
            self.isLoading = NO;
        }
        
    } failure:^(NSError *error) {
        [self.contactListView.tableView reloadData];
        [self.contactListView setAsLoading:NO animated:YES];
        self.isLoading = NO;
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Data Source
#pragma mark TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.indexSectionDictionary count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    NSString *key = [keysArray objectAtIndex:section];
    NSArray *contactArray = [self.indexSectionDictionary objectForKey:key];
    return [contactArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellID = @"ContactListTableViewCell";
    ContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[ContactListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    NSString *key = [keysArray objectAtIndex:indexPath.section];
    NSArray *contactArray = [self.indexSectionDictionary objectForKey:key];
    ContactModel *currentContact = [contactArray objectAtIndex:indexPath.row];
    [cell setContactCellWithData:currentContact];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section <= [[self.indexSectionDictionary allKeys] count]) {
        return 34.0f;
    }
    
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), 34.0f)];
    header.backgroundColor = [Util getColor:@"E8E8E8"];
    
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, 0.0f, CGRectGetWidth(header.frame) - 16.0f - 16.0f, 34.0f)];
    titleLabel.textColor = [Util getColor:@"4A4A4A"];
    titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    [header addSubview:titleLabel];
    
    if ([keysArray count] != 0) {
        titleLabel.text = [keysArray objectAtIndex:section];
    }
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] init];
    return footer;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return [keysArray objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return keysArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    return [keysArray indexOfObject:title];
}

#pragma mark - Delegate
#pragma mark UITableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *keysArray = [self.indexSectionDictionary allKeys];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    keysArray = [keysArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSString *key = [keysArray objectAtIndex:indexPath.section];
    NSArray *contactArray = [self.indexSectionDictionary objectForKey:key];
    
    ContactModel *selectedContact = [contactArray objectAtIndex:indexPath.row];
    
    ContactDetailViewController *contactDetailViewController = [[ContactDetailViewController alloc] init];
    contactDetailViewController.delegate = self;
    contactDetailViewController.contactID = selectedContact.contactID;
    [self.navigationController pushViewController:contactDetailViewController animated:YES];
}

#pragma mark AddEditContactViewController
- (void)doneAddOrEditContact {
    [self fetchContactListDataFromDatabase];
}

#pragma mark ContactDetailViewController
- (void)doneAddOrEditDataFromContactDetail {
    [self fetchContactListDataFromDatabase];
}

#pragma mark - Custom Method
- (void)addButtonDidTapped {
    if (self.isLoading) {
        return;
    }
    
    AddEditContactViewController *addEditContactViewController = [[AddEditContactViewController alloc] init];
    addEditContactViewController.delegate = self;
    [addEditContactViewController setAddEditContactViewControllerType:AddEditContactViewControllerTypeAdd];
    [self presentViewController:addEditContactViewController animated:YES completion:nil];
}

- (void)fetchContactListDataFromDatabase {
    [self.contactListView setAsLoading:YES animated:NO];
    _isLoading = YES;
    //Get database contact
    [DataManager getDatabaseContactListWithSuccess:^(NSArray *contactListDatabaseArray) {
        
        self.contactListArray = [[NSArray alloc] init];
        self.indexSectionDictionary = [[NSMutableDictionary alloc] init];
        
        NSArray *sortedDatabaseArray = [contactListDatabaseArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            ContactModel *contact1 = (ContactModel *)obj1;
            ContactModel *contact2 = (ContactModel *)obj2;
            
            NSString *firstName1 = contact1.firstName;
            firstName1 = [Util nullToEmptyString:firstName1];
            NSString *lastName1 = contact1.lastName;
            lastName1 = [Util nullToEmptyString:lastName1];
            NSString *fullName1 = [NSString stringWithFormat:@"%@ %@", firstName1, lastName1];
            
            NSString *firstName2 = contact2.firstName;
            firstName2 = [Util nullToEmptyString:firstName2];
            NSString *lastName2 = contact2.lastName;
            lastName2 = [Util nullToEmptyString:lastName2];
            NSString *fullName2 = [NSString stringWithFormat:@"%@ %@", firstName2, lastName2];
            
            return [fullName1 caseInsensitiveCompare:fullName2];
        }];
        
        self.contactListArray = sortedDatabaseArray;
        for (ContactModel *contact in self.contactListArray) {
            
            NSString *nameString = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
            NSString *firstAlphabet = [[nameString substringWithRange:NSMakeRange(0, 1)] uppercaseString];
            if ([self.alphabetSectionTitles containsObject:firstAlphabet]) {
                if ([self.indexSectionDictionary objectForKey:firstAlphabet] == nil) {
                    //No alphabet found
                    [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                }
                else {
                    //Alphabet found
                    NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:firstAlphabet] mutableCopy];
                    [contactArray addObject:contact];
                    [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                }
            }
            else {
                if ([self.indexSectionDictionary objectForKey:@"#"] == nil) {
                    //No alphabet found
                    [self.indexSectionDictionary setObject:[NSArray arrayWithObjects:contact, nil] forKey:firstAlphabet];
                }
                else {
                    //Alphabet found
                    NSMutableArray *contactArray = [[self.indexSectionDictionary objectForKey:@"#"] mutableCopy];
                    [contactArray addObject:contact];
                    [self.indexSectionDictionary setObject:contactArray forKey:firstAlphabet];
                }
            }
        }
        
        [self.contactListView.tableView reloadData];
        [self.contactListView setAsLoading:NO animated:YES];
        self.isLoading = NO;
        
    } failure:^(NSError *error) {
        [self.contactListView.tableView reloadData];
        [self.contactListView setAsLoading:NO animated:YES];
        self.isLoading = NO;
    }];
}

@end
