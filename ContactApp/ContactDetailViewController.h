//
//  ContactDetailViewController.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ContactDetailViewControllerDelegate <NSObject>

- (void)doneAddOrEditDataFromContactDetail;

@end

@interface ContactDetailViewController : BaseViewController

@property (weak, nonatomic) id <ContactDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *contactID;

@end

NS_ASSUME_NONNULL_END
