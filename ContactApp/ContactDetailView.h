//
//  ContactDetailView.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactDetailView : BaseView

@property (strong, nonatomic) UIButton *editButton;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *messageButton;
@property (strong, nonatomic) UIButton *phoneButton;
@property (strong, nonatomic) UIButton *emailButton;
@property (strong, nonatomic) UIButton *favoriteButton;

@property (strong, nonatomic) UITableView *contactDataTableView;

- (void)setContactDetailViewWithData:(ContactModel *)contact;
- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated;

@end

NS_ASSUME_NONNULL_END
