//
//  AddEditContactDataTableViewCell.m
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "AddEditContactDataTableViewCell.h"

@interface AddEditContactDataTableViewCell ()

@property (strong, nonatomic) UILabel *placeholderLabel;
@property (strong, nonatomic) UIView *separatorView;

@end

@implementation AddEditContactDataTableViewCell

#pragma mark - Lifecycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        self.contentView.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(24.0f, 0.0f, 100.0f, 55.0f)];
        self.placeholderLabel.font = [UIFont systemFontOfSize:16.0f];
        self.placeholderLabel.textAlignment = NSTextAlignmentRight;
        self.placeholderLabel.textColor = [[Util getColor:@"4A4A4A"] colorWithAlphaComponent:0.5f];
        [self.contentView addSubview:self.placeholderLabel];
        
        CGFloat dataTextFieldWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - CGRectGetMaxX(self.placeholderLabel.frame) - 32.0f - 24.0f;
        _dataTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.placeholderLabel.frame) + 32.0f, 0.0f, dataTextFieldWidth, 55.0f)];
        self.dataTextField.font = [UIFont systemFontOfSize:16.0f];
        self.dataTextField.textColor = [Util getColor:@"4A4A4A"];
        self.dataTextField.tintColor = [[Util getColor:@"4A4A4A"] colorWithAlphaComponent:0.5f];
        [self.contentView addSubview:self.dataTextField];
        
        //Screen width - left gap
        CGFloat separatorWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - 8.0f;
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(8.0f, 56.0f - 1.0f, separatorWidth , 1.0f)];
        self.separatorView.backgroundColor = [Util getColor:@"F0F0F0"];
        [self.contentView addSubview:self.separatorView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.placeholderLabel.text = @"";
    self.dataTextField.text = @"";
}

#pragma mark - Custom Method
- (void)setAddEditContactCellWithData:(ContactModel *)contact type:(AddEditContactDataTableViewCellType)type {
    if (type == AddEditContactDataTableViewCellTypeFirstName) {
        self.placeholderLabel.text = NSLocalizedString(@"First Name", @"");
        
        NSString *firstName = contact.firstName;
        firstName = [Util nullToEmptyString:firstName];
        
        if ([firstName isEqualToString:@""]) {
            self.dataTextField.text = @"-";
        }

        self.dataTextField.text = firstName;
    }
    else if (type == AddEditContactDataTableViewCellTypeLastName) {
        self.placeholderLabel.text = NSLocalizedString(@"Last Name", @"");
        
        NSString *lastName = contact.lastName;
        lastName = [Util nullToEmptyString:lastName];
        
        self.dataTextField.text = lastName;
    }
    else if (type == AddEditContactDataTableViewCellTypePhone) {
        self.placeholderLabel.text = NSLocalizedString(@"Mobile", @"");
        
        NSString *phoneNumber = contact.phoneNumber;
        phoneNumber = [Util nullToEmptyString:phoneNumber];
        
        self.dataTextField.text = phoneNumber;
    }
    else if (type == AddEditContactDataTableViewCellTypeEmail) {
        self.placeholderLabel.text = NSLocalizedString(@"Email", @"");
        
        NSString *email = contact.email;
        email = [Util nullToEmptyString:email];
        
        self.dataTextField.text = email;
    }
}

- (void)setAddEditContactCellWithType:(AddEditContactDataTableViewCellType)type {
    if (type == AddEditContactDataTableViewCellTypeFirstName) {
        self.placeholderLabel.text = NSLocalizedString(@"First Name", @"");
        self.dataTextField.placeholder = NSLocalizedString(@"First Name", @"");
    }
    else if (type == AddEditContactDataTableViewCellTypeLastName) {
        self.placeholderLabel.text = NSLocalizedString(@"Last Name", @"");
        self.dataTextField.placeholder = NSLocalizedString(@"Last Name", @"");
    }
    else if (type == AddEditContactDataTableViewCellTypePhone) {
        self.placeholderLabel.text = NSLocalizedString(@"Mobile", @"");
        self.dataTextField.placeholder = NSLocalizedString(@"Mobile", @"");
    }
    else if (type == AddEditContactDataTableViewCellTypeEmail) {
        self.placeholderLabel.text = NSLocalizedString(@"Email", @"");
        self.dataTextField.placeholder = NSLocalizedString(@"Email", @"");
    }
}

@end
