//
//  ContactDataTableViewCell.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ContactDataTableViewCellType) {
    ContactDataTableViewCellTypePhone,
    ContactDataTableViewCellTypeEmail,
};

@interface ContactDataTableViewCell : BaseTableViewCell

@property (nonatomic) ContactDataTableViewCellType contactDataTableViewCellType;

- (void)setContactCellWithData:(ContactModel *)contact type:(ContactDataTableViewCellType)type;

@end

NS_ASSUME_NONNULL_END
