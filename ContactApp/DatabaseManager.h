//
//  DatabaseManager.h
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"

@interface DatabaseManager : BaseManager

+ (DatabaseManager *)sharedManager;

+ (void)loadAllDataFromDatabaseWithQuery:(NSString *)query
                               tableName:(NSString *)tableName
                               sortByKey:(NSString *)columnName
                               ascending:(BOOL)isAscending
                                 success:(void (^)(NSArray *resultArray))success
                                 failure:(void (^)(NSError *error))failure;
+ (void)insertDataToDatabaseWithData:(NSArray *)dataArray
                           tableName:(NSString *)tableName
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure;
+ (void)insertDataToDatabaseInMainThreadWithData:(NSArray *)dataArray
                                       tableName:(NSString *)tableName
                                         success:(void (^)(void))success
                                         failure:(void (^)(NSError *error))failure;
+ (void)updateOrInsertDataToDatabaseWithData:(NSArray *)dataArray
                                   tableName:(NSString *)tableName
                                     success:(void (^)(void))success
                                     failure:(void (^)(NSError *error))failure;
+ (void)updateOrInsertDataToDatabaseInMainThreadWithData:(NSArray *)dataArray
                                               tableName:(NSString *)tableName
                                                 success:(void (^)(void))success
                                                 failure:(void (^)(NSError *error))failure;

@end
