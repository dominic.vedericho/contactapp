//
//  ContactListTableViewCell.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactListTableViewCell.h"

@interface ContactListTableViewCell ()

@property (strong, nonatomic) RNImageView *profileImageView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UIImageView *favoriteImageView;
@property (strong, nonatomic) UIView *separatorView;

@end

@implementation ContactListTableViewCell

#pragma mark - Lifecycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {        
        self.contentView.backgroundColor = [Util getColor:@"F9F9F9"];
        self.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _profileImageView = [[RNImageView alloc] initWithFrame:CGRectMake(16.0f, 12.0f, 40.0f, 40.0f)];
        self.profileImageView.layer.cornerRadius = CGRectGetWidth(self.profileImageView.frame) / 2.0f;
        self.favoriteImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.profileImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.profileImageView];
        
        //Screen width - max x position profile image view - left gap name label - right gap name label - favourite image width - right outer gap
        CGFloat nameLabelWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - CGRectGetMaxX(self.profileImageView.frame) - 16.0f - 16.0f - 16.0f - 32.0f;
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.profileImageView.frame) + 16.0f, (64.0f - 16.0f) / 2.0f, nameLabelWidth, 18.0f)];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        self.nameLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.contentView addSubview:self.nameLabel];
        
        _favoriteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds) - 32.0f - 20.0f, (64.0f - 20.0f) / 2.0f, 20.0f, 20.0f)];
        self.favoriteImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.favoriteImageView];
        
        //Screen width - left gap - right gap
        CGFloat separatorWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - 16.0f - 16.0f;
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(16.0f, 64.0f - 1.0f,separatorWidth , 1.0f)];
        self.separatorView.backgroundColor = [Util getColor:@"F0F0F0"];
        [self.contentView addSubview:self.separatorView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.profileImageView.image = [UIImage imageNamed:@"defaultPict"];
    self.nameLabel.text = @"";
    self.favoriteImageView.image = nil;
}

#pragma mark - Custom Method
- (void)setContactCellWithData:(ContactModel *)contact {
    
    NSString *firstName = contact.firstName;
    firstName = [Util nullToEmptyString:firstName];
    
    NSString *lastName = contact.lastName;
    lastName = [Util nullToEmptyString:lastName];
    
    NSString *profileImage = contact.profilePicture;
    profileImage = [Util nullToEmptyString:profileImage];
    
    BOOL isFavourite = contact.isFavorite;
    
    NSString *contactName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    //Set Data
    self.nameLabel.text = contactName;
    
    //Assuming /images/missing.png is no profile image
    if ([profileImage isEqualToString:@""] || [profileImage isEqualToString:@"/images/missing.png"]) {
        //No profile picture
        self.profileImageView.image = [UIImage imageNamed:@"defaultPict"];
    }
    else {
        [self.profileImageView setImageWithURLString:profileImage];
    }
    
    if (isFavourite) {
        self.favoriteImageView.image = [UIImage imageNamed:@"favouriteGreen"];
        
        //Screen width - max x position profile image view - left gap name label - right outer gap
        CGFloat nameLabelWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - CGRectGetMaxX(self.profileImageView.frame) - 16.0f - 16.0f;
        self.nameLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMinY(self.nameLabel.frame), nameLabelWidth, CGRectGetHeight(self.nameLabel.frame));
        
        self.favoriteImageView.frame = CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds) - 32.0f - 20.0f, (64.0f - 20.0f) / 2.0f, 20.0f, 20.0f);
    }
    else {
        self.favoriteImageView.image = nil;

        //Screen width - max x position profile image view - left gap name label - right gap name label - favourite image width - right outer gap
        CGFloat nameLabelWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - CGRectGetMaxX(self.profileImageView.frame) - 16.0f - 16.0f - 16.0f - 32.0f;
        self.nameLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMinY(self.nameLabel.frame), nameLabelWidth, CGRectGetHeight(self.nameLabel.frame));
        
        self.favoriteImageView.frame = CGRectZero;
    }
}

@end
