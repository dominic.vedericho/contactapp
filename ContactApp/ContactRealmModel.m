//
//  ContactRealmModel.m
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactRealmModel.h"

@implementation ContactRealmModel

+ (NSString *)primaryKey {
    NSString *primaryKey = @"contactID";
    return primaryKey;
}

@end
