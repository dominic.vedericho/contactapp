//
//  BaseView.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

#pragma mark - Lifecycle
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

#pragma mark - Custom Method
+ (CGRect)frameWithNavigationBar {
    return CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds) - [Util currentDeviceNavigationBarHeightWithStatusBar:YES iPhoneXLargeLayout:NO]);
}

+ (CGRect)frameWithoutNavigationBar {
    return CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
}

@end
