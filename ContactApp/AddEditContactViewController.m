//
//  AddEditContactViewController.m
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "AddEditContactViewController.h"
#import "AddEditContactView.h"
#import "AddEditContactDataTableViewCell.h"

#import <AVFoundation/AVFoundation.h>

@interface AddEditContactViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) AddEditContactView *addEditContactView;

@property (strong, nonatomic) ContactModel *contactData;

@property (strong, nonatomic) UIImageView *updatedProfilePictureImageView;
@property (strong, nonatomic) NSString *profileURLString;
@property (strong, nonatomic) NSString *firstNameString;
@property (strong, nonatomic) NSString *lastNameString;
@property (strong, nonatomic) NSString *phoneString;
@property (strong, nonatomic) NSString *emailString;

@property (nonatomic) BOOL isAddingData;

- (void)cancelButtonDidTapped;
- (void)doneButtonDidTapped;
- (void)pickImageButtonDidTapped;
- (void)cameraSourceTapped;
- (void)gallerySourceTapped;
- (void)delayCloseAnimation;

@end

@implementation AddEditContactViewController

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _addEditContactView = [[AddEditContactView alloc] initWithFrame:[BaseView frameWithoutNavigationBar]];
    [self.view addSubview:self.addEditContactView];
    
    self.addEditContactView.tableView.delegate = self;
    self.addEditContactView.tableView.dataSource = self;
    
    [self.addEditContactView.cancelButton addTarget:self action:@selector(cancelButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.addEditContactView.doneButton addTarget:self action:@selector(doneButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.addEditContactView.pickImageButton addTarget:self action:@selector(pickImageButtonDidTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _updatedProfilePictureImageView = nil;
    _profileURLString = @"";
    _firstNameString = @"";
    _lastNameString = @"";
    _phoneString = @"";
    _emailString = @"";
    
    if (self.addEditContactView.tableView.contentSize.height < self.addEditContactView.tableView.frame.size.height) {
        self.addEditContactView.tableView.scrollEnabled = NO;
    }
    else {
        self.addEditContactView.tableView.scrollEnabled = YES;
    }
    
    if(self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
        [self.addEditContactView.tableView reloadData];
        AddEditContactDataTableViewCell *firstNameCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [firstNameCell.dataTextField becomeFirstResponder];
    }
    else if(self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
        
        [self.addEditContactView setAsLoading:YES animated:NO];
        [DataManager callAPIGetContactDetailWithContactID:self.contactID success:^(ContactModel *contactData) {
            self.contactData = contactData;
            self.profileURLString = contactData.profilePicture;
            self.firstNameString = contactData.firstName;
            self.lastNameString = contactData.lastName;
            self.phoneString = contactData.phoneNumber;
            self.emailString = contactData.email;
            
            [self.addEditContactView.tableView reloadData];
            AddEditContactDataTableViewCell *firstNameCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [firstNameCell.dataTextField becomeFirstResponder];
            
            [self.addEditContactView setAsLoading:NO animated:YES];
        } failure:^(NSError *error) {
            
            [self.addEditContactView setAsLoading:NO animated:YES];
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Data Source
#pragma mark TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"AddEditContactDataTableViewCell";
    AddEditContactDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[AddEditContactDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    if (indexPath.row == 0) {
        // First Name
        if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
            [cell setAddEditContactCellWithType:AddEditContactDataTableViewCellTypeFirstName];
        }
        else if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
            [cell setAddEditContactCellWithData:self.contactData type:AddEditContactDataTableViewCellTypeFirstName];
        }
    }
    else if (indexPath.row == 1) {
        // Last Name
        if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
            [cell setAddEditContactCellWithType:AddEditContactDataTableViewCellTypeLastName];
        }
        else if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
            [cell setAddEditContactCellWithData:self.contactData type:AddEditContactDataTableViewCellTypeLastName];
        }
    }
    else if (indexPath.row == 2) {
        // Phone
        cell.dataTextField.keyboardType = UIKeyboardTypePhonePad;
        
        if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
            [cell setAddEditContactCellWithType:AddEditContactDataTableViewCellTypePhone];
        }
        else if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
            [cell setAddEditContactCellWithData:self.contactData type:AddEditContactDataTableViewCellTypePhone];
        }
    }
    else if (indexPath.row == 3) {
        // Email
        cell.dataTextField.keyboardType = UIKeyboardTypeEmailAddress;
        
        if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
            [cell setAddEditContactCellWithType:AddEditContactDataTableViewCellTypeEmail];
        }
        else if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
            [cell setAddEditContactCellWithData:self.contactData type:AddEditContactDataTableViewCellTypeEmail];
        }
    }
    
    cell.dataTextField.tag = indexPath.row;
    cell.dataTextField.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] init];
    return footerView;
}

#pragma mark - Delegate
#pragma mark UITableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark UIScrollView
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.addEditContactView endEditing:YES];
}

#pragma mark UITextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *trimmedString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    textField.text = newString;
    
    NSInteger tag = textField.tag;
    switch (tag) {
        case 0:
        {
            //First Name
            self.firstNameString = trimmedString;
            break;
        }
        case 1:
        {
            //Last Name
            self.lastNameString = trimmedString;
            break;
        }
        case 2:
        {
            //Mobile
            self.phoneString = trimmedString;
            break;
        }
        case 3:
        {
            //Email
            self.emailString = trimmedString;
            break;
        }
        default:
            break;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger tag = textField.tag;
    switch (tag) {
        case 0:
        {
            //First Name
            AddEditContactDataTableViewCell *lastNameCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            [lastNameCell.dataTextField becomeFirstResponder];
            break;
        }
        case 1:
        {
            //Last Name
            AddEditContactDataTableViewCell *phoneCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            [phoneCell.dataTextField becomeFirstResponder];
            break;
        }
        case 2:
        {
            //Mobile
            AddEditContactDataTableViewCell *emailCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
            [emailCell.dataTextField becomeFirstResponder];
            break;
        }
        case 3:
        {
            //Email
            AddEditContactDataTableViewCell *emailCell = (AddEditContactDataTableViewCell *) [self.addEditContactView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
            [emailCell.dataTextField resignFirstResponder];
            break;
        }
        default:
            break;
    }
    
    return NO;
}

#pragma mark UIImagePickerController
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //Output image
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [self.addEditContactView setProfilePictureWithImage:chosenImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Custom Method
- (void)setAddEditContactViewControllerType:(AddEditContactViewControllerType)addEditContactViewControllerType {
    _addEditContactViewControllerType = addEditContactViewControllerType;
}

- (void)cancelButtonDidTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneButtonDidTapped {
    
    if(self.isAddingData) {
        return;
    }
    
    self.isAddingData = YES;
    
    //Validation
    if (self.firstNameString == nil || [self.firstNameString isEqualToString:@""]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Please fill first name" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([self.firstNameString length] < 2) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Please fill first name (minimum is 2 characters)" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (self.lastNameString == nil || [self.lastNameString isEqualToString:@""]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Please fill last name" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([self.lastNameString length] < 2) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Please fill last name (minimum is 2 characters)" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (self.phoneString == nil || [self.phoneString isEqualToString:@""]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Please fill the phone number" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if ([self.phoneString length] < 10) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"Phone number must be at least 10 digits" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.isAddingData = NO;
        }];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeAdd) {
            //Add the data
            ContactModel *newContact = [ContactModel new];
            newContact.firstName = self.firstNameString;
            newContact.lastName = self.lastNameString;
            newContact.phoneNumber = self.phoneString;
            newContact.email = self.emailString;
            newContact.isFavorite = NO;
            
            [self.addEditContactView endEditing:YES];
            [self.addEditContactView setAsLoading:YES animated:YES];
            [DataManager callAPIAddNewContactWithData:newContact success:^(ContactModel *contactData) {
                //Save to database
                [DataManager updateOrInsertContactToDatabaseWithData:@[contactData] tableName:@"ContactRealmModel" success:^{
                    
                    if ([self.delegate respondsToSelector:@selector(doneAddOrEditContact)]) {
                        [self.delegate doneAddOrEditContact];
                    }
                    
                    self.isAddingData = NO;
                    [self.addEditContactView setAsLoading:NO animated:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                } failure:^(NSError *error) {
                    self.isAddingData = NO;
                    [self.addEditContactView setAsLoading:NO animated:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            } failure:^(NSError *error) {
                
                [self.addEditContactView setAsLoading:NO animated:YES];
                
                UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.isAddingData = NO;
                }];
                
                [alertController addAction:okAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }];
        }
        else if (self.addEditContactViewControllerType == AddEditContactViewControllerTypeEdit) {
            NSString *firstNameString = self.contactData.firstName;
            NSString *lastNameString = self.contactData.lastName;
            NSString *phoneString = self.contactData.phoneNumber;
            NSString *emailString = self.contactData.email;
            
            if (self.updatedProfilePictureImageView.image == nil &&
                [firstNameString isEqualToString:self.firstNameString] &&
                [lastNameString isEqualToString:self.lastNameString] &&
                [phoneString isEqualToString:self.phoneString] &&
                [emailString isEqualToString:self.emailString]
                ) {
                //Data is still the same, no need to update
                self.isAddingData = NO;
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else {
                //Update the data
                ContactModel *updatedContact = [ContactModel new];
                updatedContact.firstName = self.firstNameString;
                updatedContact.lastName = self.lastNameString;
                updatedContact.phoneNumber = self.phoneString;
                updatedContact.email = self.emailString;
                
                [self.addEditContactView endEditing:YES];
                [self.addEditContactView setAsLoading:YES animated:YES];
                [DataManager callAPIUpdateContactWithContactID:self.contactID contactData:updatedContact success:^(ContactModel *contactData) {
                    //Save to database
                    [DataManager updateOrInsertContactToDatabaseWithData:@[contactData] tableName:@"ContactRealmModel" success:^{
                        if ([self.delegate respondsToSelector:@selector(doneAddOrEditContact)]) {
                            [self.delegate doneAddOrEditContact];
                        }
                        
                        [self performSelector:@selector(delayCloseAnimation) withObject:nil afterDelay:1.0f];
                        
                    } failure:^(NSError *error) {
                        self.isAddingData = NO;
                        [self.addEditContactView setAsLoading:NO animated:YES];
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                } failure:^(NSError *error) {
                    
                    [self.addEditContactView setAsLoading:NO animated:YES];
                    
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:error.domain preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        self.isAddingData = NO;
                    }];
                    
                    [alertController addAction:okAction];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                }];
            }
        }
    }
}

- (void)pickImageButtonDidTapped {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Choose Profile Image Source" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             // Camera button tapped
                                                             [self cameraSourceTapped];
                                                         }];
    
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             // Gallery button tapped
                                                             [self gallerySourceTapped];
                                                         }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             // Cancel button tapped
                                                         }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:galleryAction];
    [actionSheet addAction:cancelAction];
 
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)cameraSourceTapped {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Failed" message:@"The app has been restricted from using the camera, please allow camera access to this app." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)gallerySourceTapped {
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}

- (void)delayCloseAnimation {
    self.isAddingData = NO;
    [self.addEditContactView setAsLoading:NO animated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
