//
//  DatabaseManager.m
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "DatabaseManager.h"
#import <Realm/Realm.h>
#import "BaseRealmModel.h"

@interface DatabaseManager ()

- (NSArray *)convertRealmResultIntoArray:(RLMResults *)results;
- (id)convertDictionaryIntoRealmObjectWithData:(NSDictionary *)dataDictionary tableName:(NSString *)tableName;
- (NSData *)getKey;
- (RLMRealm *)createRealm;
+ (RLMResults *)filterResultsWithWhereClauseQuery:(NSString *)whereClauseQuery
                                          results:(RLMResults *)results;
+ (RLMResults *)sortResultsWithColumnName:(NSString *)columnName
                              isAscending:(BOOL)isAscending
                                  results:(RLMResults *)results;

@end

@implementation DatabaseManager

#pragma mark - Lifecycle
+ (DatabaseManager *)sharedManager {
    static DatabaseManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (id)init {
    self = [super init];
    
    if (self) {

    }
    
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - Custom Method
- (NSArray *)convertRealmResultIntoArray:(RLMResults *)results {
    NSMutableArray *resultDataArray = [NSMutableArray array];
    
    for (BaseRealmModel *object in results) {
        NSDictionary *resultDictionary = [object toDictionary];
        [resultDataArray addObject:resultDictionary];
    }
    
    return resultDataArray;
}

- (id)convertDictionaryIntoRealmObjectWithData:(NSDictionary *)dataDictionary tableName:(NSString *)tableName {
    Class databaseClass = NSClassFromString(tableName);
    id resultRealmModel = [[databaseClass alloc] initWithDictionary:dataDictionary error:nil];
    return resultRealmModel;
}

- (NSData *)getKey {
    NSString *keyString = @"1234567890123456789012345678901234567890123456789012345678901234";
    NSData *key = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    return key;
}

- (RLMRealm *)createRealm {
    RLMRealmConfiguration *configuration = [RLMRealmConfiguration defaultConfiguration];
    configuration.encryptionKey = [[DatabaseManager sharedManager] getKey];
    RLMRealm *realm = [RLMRealm realmWithConfiguration:configuration
                                                 error:nil];
    return realm;
}

+ (RLMResults *)filterResultsWithWhereClauseQuery:(NSString *)whereClauseQuery
                                          results:(RLMResults *)results {
    
    results = [results objectsWhere:whereClauseQuery];
    return results;
}

+ (RLMResults *)sortResultsWithColumnName:(NSString *)columnName
                              isAscending:(BOOL)isAscending
                                  results:(RLMResults *)results {
    results = [results sortedResultsUsingKeyPath:columnName ascending:isAscending];
    return results;
}


+ (void)loadAllDataFromDatabaseWithQuery:(NSString *)query
                               tableName:(NSString *)tableName
                               sortByKey:(NSString *)columnName
                               ascending:(BOOL)isAscending
                                 success:(void (^)(NSArray *resultArray))success
                                 failure:(void (^)(NSError *error))failure {
    RLMRealm *realm = [[DatabaseManager sharedManager] createRealm];
    
    RLMResults *results = [NSClassFromString(tableName) allObjectsInRealm:realm];
    
    if (![query isEqualToString:@""]) {
        results = [results objectsWhere:query];
    }
    
//    results = [results distinctResultsUsingKeyPaths:@[columnName]];
    results = [results sortedResultsUsingKeyPath:columnName ascending:isAscending];
    
    NSArray *resultArray = [NSArray array];
    resultArray = [[DatabaseManager sharedManager] convertRealmResultIntoArray:results];
    
    [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
        // handle error
        failure(error);
    }];
    
    success(resultArray);
}

+ (void)insertDataToDatabaseWithData:(NSArray *)dataArray
                           tableName:(NSString *)tableName
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure {
    
    if ([dataArray count] <= 0) {
        [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
            // handle error
            failure(error);
        }];
        
        success();
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
     dispatch_async(queue, ^{
         @autoreleasepool {
             NSMutableArray *resultArray = [NSMutableArray array];
             for (NSDictionary *dataDictionary in dataArray) {
                 id resultRealmModel = [[DatabaseManager sharedManager] convertDictionaryIntoRealmObjectWithData:dataDictionary tableName:tableName];
                 [resultArray addObject:resultRealmModel];
             }
             
             RLMRealm *realm = [[DatabaseManager sharedManager] createRealm];
             
             [realm beginWriteTransaction];
             [realm addObjects:resultArray];
             [realm commitWriteTransaction];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
                     // handle error
                     failure(error);
                 }];
                 
                 success();
             });
        }
    });
}

+ (void)insertDataToDatabaseInMainThreadWithData:(NSArray *)dataArray
                                       tableName:(NSString *)tableName
                                         success:(void (^)(void))success
                                         failure:(void (^)(NSError *error))failure {
    if ([dataArray count] <= 0) {
        success();
    }
    
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSDictionary *dataDictionary in dataArray) {
        id resultRealmModel = [[DatabaseManager sharedManager] convertDictionaryIntoRealmObjectWithData:dataDictionary tableName:tableName];
        [resultArray addObject:resultRealmModel];
    }
    
    RLMRealm *realm = [[DatabaseManager sharedManager] createRealm];
    
    [realm beginWriteTransaction];
    [realm addObjects:resultArray];
    [realm commitWriteTransaction];

    [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
        // handle error
        failure(error);
    }];

    success();
}

+ (void)updateOrInsertDataToDatabaseWithData:(NSArray *)dataArray
                                   tableName:(NSString *)tableName
                                     success:(void (^)(void))success
                                     failure:(void (^)(NSError *error))failure {
    if ([dataArray count] <= 0) {
        success();
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @autoreleasepool {
            NSMutableArray *resultArray = [NSMutableArray array];
            for (NSDictionary *dataDictionary in dataArray) {
                id resultRealmModel = [[DatabaseManager sharedManager] convertDictionaryIntoRealmObjectWithData:dataDictionary tableName:tableName];
                [resultArray addObject:resultRealmModel];
            }
            
            RLMRealm *realm = [[DatabaseManager sharedManager] createRealm];
            
            [realm beginWriteTransaction];
            [realm addOrUpdateObjects:resultArray];
            [realm commitWriteTransaction];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
                // handle error
                failure(error);
            }];
            
            success();
        });
    });
}

+ (void)updateOrInsertDataToDatabaseInMainThreadWithData:(NSArray *)dataArray
                                               tableName:(NSString *)tableName
                                                 success:(void (^)(void))success
                                                 failure:(void (^)(NSError *error))failure {
    if ([dataArray count] <= 0) {
        success();
    }
    
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSDictionary *dataDictionary in dataArray) {
        id resultRealmModel = [[DatabaseManager sharedManager] convertDictionaryIntoRealmObjectWithData:dataDictionary tableName:tableName];
        [resultArray addObject:resultRealmModel];
    }
    
    RLMRealm *realm = [[DatabaseManager sharedManager] createRealm];
    
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:resultArray];
    [realm commitWriteTransaction];
    
    [[RLMSyncManager sharedManager] setErrorHandler:^(NSError *error, RLMSyncSession *session) {
        // handle error
        failure(error);
    }];
    
    success();
}

@end
