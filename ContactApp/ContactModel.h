//
//  ContactModel.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactModel : BaseModel

@property (strong, nonatomic) NSString *contactID;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *profilePicture;
@property (nonatomic) BOOL isFavorite;
@property (strong, nonatomic) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
