//
//  BaseRealmModel.h
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import <Realm/Realm.h>

@interface BaseRealmModel : RLMObject

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError **)err;
- (NSDictionary *)toDictionary;

@end
