//
//  AddEditContactDataTableViewCell.h
//  ContactApp
//
//  Created by Dominic Vedericho on 05/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, AddEditContactDataTableViewCellType) {
    AddEditContactDataTableViewCellTypeFirstName,
    AddEditContactDataTableViewCellTypeLastName,
    AddEditContactDataTableViewCellTypePhone,
    AddEditContactDataTableViewCellTypeEmail,
};

@interface AddEditContactDataTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UITextField *dataTextField;
@property (nonatomic) AddEditContactDataTableViewCellType addEditContactDataTableViewCellType;

- (void)setAddEditContactCellWithData:(ContactModel *)contact type:(AddEditContactDataTableViewCellType)type;
- (void)setAddEditContactCellWithType:(AddEditContactDataTableViewCellType)type;

@end

NS_ASSUME_NONNULL_END
