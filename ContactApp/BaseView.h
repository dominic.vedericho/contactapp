//
//  BaseView.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseView : UIView

+ (CGRect)frameWithNavigationBar;
+ (CGRect)frameWithoutNavigationBar;

@end

NS_ASSUME_NONNULL_END
