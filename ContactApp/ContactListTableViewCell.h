//
//  ContactListTableViewCell.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactListTableViewCell : BaseTableViewCell

- (void)setContactCellWithData:(ContactModel *)contact;

@end

NS_ASSUME_NONNULL_END
