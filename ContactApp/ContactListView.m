//
//  ContactListView.m
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#import "ContactListView.h"

@interface ContactListView ()

@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;

@end

@implementation ContactListView

#pragma mark - Lifecycle
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)) style:UITableViewStyleGrouped];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self.tableView setSectionIndexColor:[Util getColor:@"4A4A4A"]];
        [self addSubview:self.tableView];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
        self.loadingView.alpha = 0.0f;
        self.loadingView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self addSubview:self.loadingView];
        
        _indicatorView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.loadingView.frame) - 20.0f) / 2.0f, (CGRectGetHeight(self.loadingView.frame) - 20.0f) / 2.0f, 20.0f, 20.0f)];
        self.indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.indicatorView startAnimating];
        [self.loadingView addSubview:self.indicatorView];
    }
    
    return self;
}

#pragma mark - Custom Method
- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated {
    if (isAnimated) {
        if (isLoading) {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 1.0f;
            }];
        }
        else {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 0.0f;
            }];
        }
    }
    else {
        if (isLoading) {
            self.loadingView.alpha = 1.0f;
        }
        else {
            self.loadingView.alpha = 0.0f;
        }
    }
}

@end
